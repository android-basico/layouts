package com.example.linearlayout;

import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class FrameLayout extends AppCompatActivity {

    private ImageView iv1;
    private Button btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_frame_layout);

        iv1 = (ImageView) findViewById(R.id.iv_imagen);
        btn = (Button) findViewById(R.id.btn_ocultar);
    }

    public void ocultar(View view) {
        btn.setVisibility(View.INVISIBLE);
        iv1.setVisibility(View.VISIBLE);
    }
    public void mostrar(View view) {
        btn.setVisibility(View.VISIBLE);
        iv1.setVisibility(View.INVISIBLE);
    }

}
