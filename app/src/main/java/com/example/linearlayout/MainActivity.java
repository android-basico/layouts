package com.example.linearlayout;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void linearLayout(View view) {
        Intent linearLayout = new Intent(this, LinearLayout.class);
        startActivity(linearLayout);
    }

    public void tableLayout(View view) {
        Intent tableLayout = new Intent(this, TableLayout.class);
        startActivity(tableLayout);
    }

    public void frameLayout(View view) {
        Intent frameLayout = new Intent(this, FrameLayout.class);
        startActivity(frameLayout);
    }

    public void scrollView(View view) {
        Intent scrollView = new Intent(this, ScrollView.class);
        startActivity(scrollView);
    }

}
