package com.example.linearlayout;

import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.Toast;

public class ScrollView extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scroll_view);
    }
    public void seleccion (View view) {
        switch (view.getId()) {
            case R.id.btn_banana:
                Toast.makeText(this, "Esto es una banana", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btn_cereza:
                Toast.makeText(this, "Esto es una cereza", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btn_franburguesa:
                Toast.makeText(this, "Esto es una framburguesa", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btn_fresa:
                Toast.makeText(this, "Esto es una fresa", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btn_zarzamora:
                Toast.makeText(this, "Esto es una zarzamora", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btn_kiwi:
                Toast.makeText(this, "Esto es un kiwi", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btn_mango:
                Toast.makeText(this, "Esto es un mango", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btn_manzana:
                Toast.makeText(this, "Esto es una manzana", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btn_melon:
                Toast.makeText(this, "Esto es un melon", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btn_naranja:
                Toast.makeText(this, "Esto es una naranja", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btn_pera:
                Toast.makeText(this, "Esto es una pera", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btn_pina:
                Toast.makeText(this, "Esto es una piña", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btn_sandia:
                Toast.makeText(this, "Esto es una sandia", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btn_uvas:
                Toast.makeText(this, "Esto es una uva", Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
